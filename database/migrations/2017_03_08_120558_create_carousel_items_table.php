<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarouselItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carousel_items', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('carousel_id')->unsigned();
            $table->foreign('carousel_id')
                  ->references('id')
                  ->on('carousels')                               
                  ->onDelete('cascade');
            
            $table->string('title');
            $table->enum('title_position', array('left', 'right', 'center'))->nullable();
            $table->string('image_extension', 4)->nullable();
            $table->integer('order')->unsigned()->default(0);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carousels_items');
    }
}
