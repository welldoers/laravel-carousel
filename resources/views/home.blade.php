@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Home page</div>

                <div class="panel-body">
                    <a href="{{ route('admin.home') }}">Administration</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
