@extends('admin.layouts.app')

@section('content')
<div class="container">
    <h1>{{ $pageTitle }}</h1> 

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">{{$item->id ? 'Update item' : 'Create item'}}</div>
                <div class="panel-body">
                    {{ Form::model($item, array('route' => array($module . '.update', $item->id), 'method' => 'PUT', 'files' => 'true')) }}

                    @yield('editFormFields')
                    
                    <a href="{{route($module . '.index')}}#grid" class="btn btn-small btn-danger">Cancel</a>
                    <a href="{{route(Route::currentRouteName(), ['id' => $item->id])}}" class="btn btn-small btn-warning">Reset</a>
                    {{ Form::submit('Save', array('class' => 'btn btn-small btn-success')) }}

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
