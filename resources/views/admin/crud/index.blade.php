@extends('admin.layouts.app')

@section('content')
<div class="container">
    <h1>{{ $pageTitle }}</h1>
                        
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Filter</div>
                <div class="panel-body">@yield('filterForm')</div>
            </div>
        </div>
    </div>
    
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <a name="grid"></a>
            @yield('grid')
        <div>
    </div>

    {{ $items->links() }}
</div>
@endsection