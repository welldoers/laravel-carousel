@extends('admin.crud.edit')

@section('editFormFields')

    <div class="form-group{{ $errors->has('carousel_id') ? ' has-error' : '' }}">
        {{ Form::label('carousel_id', 'Carousel') }}
        {{ Form::select('carousel_id', array('' => 'Select') + $data['carousels'], $item->carousel_id, array('class' => 'form-control', 'id' => 'carousel_id')) }}
        @if ($errors->has('carousel_id'))
            <span class="help-block">
                <strong>{{ $errors->first('carousel_id') }}</strong>
            </span>
        @endif
    </div>

    
    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', $item->title, array('class' => 'form-control', 'id' => 'title')) }}

        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('title_position') ? ' has-error' : '' }}">
        {{ Form::label('title_position', 'Title Position') }}
        {{ Form::select('title_position', array('' => 'Select') + $data['title_positions'], $item->title_position, array('class' => 'form-control', 'id' => 'carousel_id')) }}
        @if ($errors->has('title_position'))
            <span class="help-block">
                <strong>{{ $errors->first('title_position') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('image_extension') ? ' has-error' : '' }}">
        {{ Form::label('image_extension', 'Image') }}
        {{ Form::file('image_extension') }}
        @if ($errors->has('image_extension'))
            <span class="help-block">
                <strong>{{ $errors->first('image_extension') }}</strong>
            </span>
        @endif
        
        @if ($item->getImageSrc('medium'))
        <img src="{{ $item->getImageSrc('medium') }}" alt="{{ $item->title }}"/>
        @endif
    </div>

    <div class="form-group{{ $errors->has('order') ? ' has-error' : '' }}">
        {{ Form::label('order', 'Order') }}
        {{ Form::number('order', $item->order, array('class' => 'form-control form-control-number', 'id' => 'order', 'min' => 0)) }}
        @if ($errors->has('order'))
            <span class="help-block">
                <strong>{{ $errors->first('order') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">                       
        <div class="checkbox">
            <label for="active">
            {{ Form::hidden('active', 0)}}
            {{ Form::checkbox('active', 1, $item->active == 1, array('id' => 'title'))}}
            Active
            </label>
        </div>
    </div>  
@endsection