@extends('admin.crud.index')

@section('pageTitle')
    Carousel items
@endsection

@section('filterForm')

    {{ Form::open(array('route' => ['carousel-items.index', '#grid'], 'method' => 'GET')) }}
    <div class="form-group">
        {{ Form::label('carousel_id', 'Carousel') }}
        {{ Form::select('filters[carousel_id]', array('' => 'Select') + $data['carousels'], Input::get('filters.carousel_id'), array('class' => 'form-control', 'id' => 'carousel_id')) }}
    </div>
    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('filters[title]', Input::get('filters.title'), array('class' => 'form-control', 'id' => 'title')) }}
    </div>
    <div class="form-group">
        {{ Form::label('title_position', 'Title Position') }}
        {{ Form::select('filters[title_position]', array('' => 'Select') + $data['title_positions'], Input::get('filters.title_position'), array('class' => 'form-control', 'id' => 'carousel_id')) }}
    </div>
    <div class="form-group">
        <label>Status:</label>
        <div class="radio">
          <label for="active--1">
            {{ Form::radio('filters[active]', -1, Input::get('filters.active') == -1 || !Input::get('filters'), array('id' => 'active--1')) }}                            
            All
          </label>
          <label for="active-1">
            {{ Form::radio('filters[active]', 1, Input::get('filters.active') == 1, array('id' => 'active-1')) }}                            
            Active
          </label>
          <label for="active-0">
            {{ Form::radio('filters[active]', 0, Input::get('filters') && Input::get('filters.active') == 0, array('id' => 'active-0')) }}                            
            Inactive
          </label>
        </div>
    </div>
    {{ Form::submit('Filter', array('class' => 'btn btn-small btn-success')) }}
    <a href="{{route(Route::currentRouteName())}}#grid" class="btn btn-small btn-success">Reset</a>

    {{ Form::close() }}
@endsection

@section('grid')

<div class="grid-buttons">
    <a href="{{route('carousel-items.create')}}" class="btn btn-small btn-success pull-right">Add new</a>
</div>        

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th><a href="{{get_sort_link('id')}}">ID</a></th>               
            <th><a href="{{get_sort_link('carousel')}}">Carousel</a></th>
            <th><a href="{{get_sort_link('title')}}">Image</a></th>
            <th><a href="{{get_sort_link('title_position')}}">Title Position</a></th>
            <th><a href="{{get_sort_link('order')}}">Order</a></th>
            <th><a href="{{get_sort_link('active')}}">Status</a></th>
            <th><a href="{{get_sort_link('created_at')}}">Created At</a></th>
            <th><a href="{{get_sort_link('updated_at')}}">Updated At</a></th>            
            <th>Actions</th>            
        </tr>
    </thead>
    <tbody>
    @foreach($items as $key => $value)
        <tr>            
            <td>{{ $value->id }}</td>
            <td><a href="{{route('carousels.edit', ['id' => $value->carousel->id])}}">{{ $value->carousel->title }}</a></td>
            <td><a href="{{route('carousel-items.edit', ['carousel' => $value->id])}}">{{ $value->title }}<br/><img src="{{ $value->getImageSrc('thumb') }}" alt="{{ $value->title }}"/></a></td>
            <td>{{ $value->title_position }}</td>
            <td>{{ $value->order }}</td>
            <td><span class="btn {{ $value->active ? 'btn-success' : 'btn-danger' }}">{{ $value->active ? 'active' : 'inactive' }}</span></td>
            <td>{{ $value->getDatetimeColumn('created_at') }}</td>
            <td>{{ $value->getDatetimeColumn('updated_at') }}</td>
            <td>
                <a class="btn btn-success grid-button" href="{{route('carousel-items.edit', ['carousel' => $value->id])}}">Edit</a>
                {{ Form::open(array('route' => ['carousel-items.destroy', $value->id], 'class' => 'grid-button')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection