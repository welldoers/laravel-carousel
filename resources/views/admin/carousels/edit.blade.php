@extends('admin.crud.edit')

@section('editFormFields')
    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', $item->title, array('class' => 'form-control', 'id' => 'title')) }}

        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">                       
        <div class="checkbox">
            <label for="active">
            {{ Form::hidden('active', 0)}}
            {{ Form::checkbox('active', 1, $item->active == 1, array('id' => 'title'))}}
            Active
            </label>
        </div>
    </div>  
@endsection