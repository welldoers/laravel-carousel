@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Admin section</div>
                <div class="panel-body">
                    <nav class="admin-main-nav">
                        <ul>
                            <li><a href="{{route('carousels.index')}}">Carousels mamagement</a>  
                                <ul>
                                    <li><a href="{{route('carousels.index')}}">Carousels listing</a></li>
                                    <li><a href="{{route('carousels.create')}}">Create carousel</a></li>
                                </ul>
                            </li>
                            <li><a href="{{route('carousel-items.index')}}">Carousel items mamagement</a>  
                                <ul>
                                    <li><a href="{{route('carousel-items.index')}}">Carousel items listing</a></li>
                                    <li><a href="{{route('carousel-items.create')}}">Create carousel item</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
