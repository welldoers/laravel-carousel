# Да се направи функционалност за управление на [carousel](http://kenwheeler.github.io/slick/) в Laravel;
---
## Приложението да включва:
* вградения в Laravel auth scaffold-инг;
* admin панел и front част;
* в admin панела да има CRUD операции със съответните валидации за Carousel и неговите Item-и;
* всеки Item да има следните полета (освен стандартните primary_key/foreign_key или timestamps):
	* заглавие
	* позиция на заглавието, напр. в ляво/в центъра/в дясно
	* изображение (За качването и манипулацията на снимки може да се ползва [този пакет](http://image.intervention.io/))
	* пореден номер (позиция в carousel-а)
* да се направят migrations и seed-ъри за моделите;
* във front end-а да може да се виждат (и да функционират) carousel-ите, а ако няма да има някакво съобщение с линк, който да води към admin панела;
---
> Да се използва това [repo](https://bitbucket.org/welldoers/laravel-carousel/overview).