<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');


Route::group(
[
    'namespace'     => 'Admin', 
    'prefix'        => 'admin',
    'middleware'    => 'auth.admin'
], 
    function () {

        /**
         * Login
         */
        Route::get('', 'HomeController@index')->name('admin.home');
        Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
        Route::post('/login', 'LoginController@login')->name('admin.checklogin');
        Route::post('/logout', 'LoginController@logout')->name('admin.logout');

        Route::resource('carousels', 'CarouselsController');
        Route::resource('carousel-items', 'CarouselItemsController');
    }
);
