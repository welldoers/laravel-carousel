<?php

namespace App\Lib\Model\Traits;

use Image as Img;
use Illuminate\Support\Facades\URL;

trait Image 
{
    protected $imageSizes = array();
    protected $imagePath = null;
    protected $imageExtensionProperty = null;
    protected $idProperty = null;
        
    public function setImageSizes(array $sizes)
    {
        $this->imageSizes = $sizes;
        return $this;
    }
    
    public function setImagePath($path)
    {
        $this->imagePath = $path;
    }
    
    public function clearImages($removePath = false) 
    {
        $path = $this->getImagePath(true);
        
        $dir = new \DirectoryIterator($this->getImagePath(true));
        
        foreach ($dir as $file) {
            if (!$file->isDir() && !$file->isDot()) {
                unlink($file->getPathname());
            }
        }
        
        if ($removePath) {
            rmdir($path);
        }
    }
    
    public function getImageName($thumb = 'original')
    {
        return $thumb . '.' . $this->{$this->imageExtensionProperty};
    }
    
    public function getImagePath($createIfNotExist = false, $thumb = false)
    {
        $path = public_path() . '/' 
            . $this->imagePath . '/' 
            . $this->{$this->idProperty} . '/'
        ;
        
        if ($createIfNotExist && !file_exists($path)) {
            mkdir($path);
        }
        
        if (!$thumb) {
            return $path;
        }
        
        return $path . $this->getImageName($thumb);
    }
    
    public function storeImages($image)
    {   
        $this->clearImages();

        $image->move($this->getImagePath(true), $this->getImageName());

        foreach ($this->imageSizes as $name => $dimensions) {
            $thumb = Img::make($this->getImagePath() . $this->getImageName());
            $thumb->resize(
                $dimensions['width'], 
                $dimensions['height'], 
                function ($constraint) {
                    $constraint->aspectRatio();
                });
            $thumb->save($this->getImagePath() . $this->getImageName($name));
        }
    }
    
    public function getImageSrc($thumb = 'original')
    {
        if (!file_exists($this->getImagePath(false, $thumb))) {
            return false;
        }
        
        return URL::to('/') . '/' 
                . $this->imagePath . '/' 
                . $this->{$this->idProperty} . '/' 
                . $this->getImageName($thumb)
        ;
    }
}

