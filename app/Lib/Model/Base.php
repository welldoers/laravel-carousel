<?php

namespace App\Lib\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Input;

class Base extends Model
{
    public function __construct(array $attributes = array()) 
    {
        parent::__construct($attributes);
        
        /**
         * @todo - should add separate config value
         */
        if (config('app.env') == 'local') {
            DB::enableQueryLog();
        }
    }

    public static function getTableName()
    {
        return (new static)->getTable();
    }

    
    public function getAllColumns()
    {
        return Schema::getColumnListing($this->getTable());
    }
    
    public function prepareQuery(array $filters = array())
    {
        return $this->query();
    }

    public function fetchForListing(
        array $filters = array(), 
        $orderField = null, 
        $orderDir = null
    ) {
        
        $query = $this->prepareQuery($filters);
  
        $columns = $this->getAllColumns();
        
        foreach ($filters as $field => $value) {
            if (in_array($field, $columns)) {
                
                if (substr($field, -3) != '_id') {
                    $query->where($field, 'LIKE', str_replace('*', '%', $value));
                } else {
                    $query->where($field, '=', $value);
                }
            }
        }
        
        if (empty($orderField) || !in_array($orderField, $columns)) {
            $orderField = $this->primaryKey;
        }
        
        $query->orderBy($orderField, ($orderDir != 'desc') ? 'asc' : 'desc');
        
        $items = $query->paginate($this->perPage);
        
        $items->appends(Input::except('page'));
        
        return $items;
    }
    
    
    public function setFromArray(array $properties)
    {
        $columns = $this->getAllColumns();
        
        foreach ($properties as $key => $value) {
            if (in_array($key, $columns)) { 
                $this->$key = $value;
            }
        }
        
        return $this;
    }
    
    public function getDatetimeColumn($column, $formatted = true)
    {
        if (!in_array($column, $this->getAllColumns())) {
            return null;
        }
        
        if (!$formatted) {
            return $this->$column;
        }
        
        /**
         * @todo add the format in configuration file
         */
        $format = 'd.m.Y H:i';
        
        return $this->$column ? $this->$column->format($format) : null;
    }
    
    public function getPairs($keyField, $valueField)
    {
        $pairs = $this->query()
                      ->get()
                      ->pluck($valueField, $keyField)->toArray();
        
        return $pairs;
    }
    
}