<?php

use Illuminate\Support\Facades\Input;

if (!function_exists('get_sort_link')) {
    function get_sort_link($orderField)
    {
        $params = Input::get();
        
        $orderDir = Input::get('order_dir');
        $prevOrderField = Input::get('order_field');
        
        if (!$orderDir || !in_array($orderDir, array('asc', 'desc'))) {
            $orderDir = 'asc';
        }
        
        if ($prevOrderField && $prevOrderField == $orderField) {
            $orderDir = $orderDir == 'asc' ? 'desc' : 'asc';
        } 
        
        $params['order_field'] = $orderField;
        $params['order_dir'] = $orderDir;
        
        return '?' . http_build_query($params) . '#grid';
    }
}


if (!function_exists('p')) {
    function p()
    {
        foreach (func_get_args() as $arg) {
            echo '<pre>';
            print_r($arg);
            echo '</pre>';
        }
    }
}

