<?php

namespace App\Lib;

class Utils 
{
    public static function camelCaseToDash($string)
    {        
        $str = lcfirst($string);
        return preg_replace_callback(
            '/([A-Z])/', 
            function($matches) {
                return '-' . strtolower($matches[0]);
            },
            $str
        );        
    }
    
    public static function camelCaseToUnderscore($string)
    {
        $str = lcfirst($string);
        return preg_replace_callback(
            '/([A-Z])/', 
            function($matches) {
                return '_' . strtolower($matches[0]);
            },
            $str
        );        
    }

    public static function dashToCamelCase($string)
    {        
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    }    
    
    public static function underscoreToCamelCase($word)
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', $word)));
    }
    
}
