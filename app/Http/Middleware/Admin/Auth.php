<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Illuminate\Support\Facades\Auth as AuthFacade;

class Auth
{
    
    const LOGIN_ROUTE = 'admin.login';
    const HOME_ROUTE = 'admin.home';
    const CHECK_LOGIN_ROUTE = 'admin.checklogin';
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        //app('router')->getRoutes()->getByName(self::LOGIN_ROUTE);
        $isLoginPage = \Request::route()->getName() == self::LOGIN_ROUTE ||
                \Request::route()->getName() == self::CHECK_LOGIN_ROUTE
        ;
       
        if (AuthFacade::check()) {        
            
            if ($isLoginPage) {
                return redirect()->route(self::HOME_ROUTE);
            }
           
            return $next($request);
            
        } 
        
        if (!$isLoginPage) {
            return redirect()->route(self::LOGIN_ROUTE);
        }
        
  
        return $next($request);
    }
    
    
}
