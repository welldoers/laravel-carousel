<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CrudController;
use App\Models\Carousel;

class CarouselsController extends CrudController
{

    /**
     * Create a new controller instance.
     *
     * @param Carousel $model
     * @return void
     */
    public function __construct(Carousel $model)
    {
        parent::__construct();
        
        $this->model = $model;
    }
    
    protected function getEditFormRules($id) 
    {
        return array(
            'title' => 'required'
        );
    }
}
