<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Middleware\Admin\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{        
    use AuthenticatesUsers;

    public function showLoginForm()
    {
        return view('admin.login.index');
    }
    
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        
        return redirect()->route(Auth::HOME_ROUTE);
    }
    
    public function redirectPath()
    {
        return route(Auth::LOGIN_ROUTE);
    }

}
