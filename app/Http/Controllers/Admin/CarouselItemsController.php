<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CrudController;
use App\Models\CarouselItem;
use App\Models\Carousel;

class CarouselItemsController extends CrudController
{
    
    protected $pageTitle = 'Carousel items';
    
    /**
     * Create a new controller instance.
     *
     * @param CarouselItem $model
     * @return void
     */
    public function __construct(CarouselItem $model)
    {
        parent::__construct();
        
        $this->model = $model;
    }    
    
    protected function getViewData()
    {
        return array(
            'carousels'         => (new Carousel())->getPairs('id', 'title'),
            'title_positions'   => $this->model->getPositions()
        );
    }
    
    protected function getEditFormRules($id) 
    {
        $rules = array(
            'title'             => 'required',
            'carousel_id'       => 'required',
            'title_position'    => 'required',
            'order'             => 'integer|min:0',
            'image_extension'   => ''
        );
        
        if (!$id) {
            $rules['image_extension'] = 'required|';
        }
        
        $rules['image_extension'] .= 'image';
        
        return $rules;
    }
}
