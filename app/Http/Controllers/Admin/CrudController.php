<?php

namespace App\Http\Controllers\Admin;

#use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lib\Utils;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

abstract class CrudController extends Controller
{

    /**
     *
     * @var App\Lib\Model\Base
     */
    protected $model = null;

    protected $moduleName = null;
    
    protected $pageTitle = null;
    
    public function __construct() 
    {
        $this->moduleName = $this->getModuleName();
        
        if (!$this->pageTitle) {
            $this->pageTitle = ucfirst($this->moduleName);
        }
        
        View::share('module', $this->moduleName);
        View::share('pageTitle', $this->pageTitle);
    }

    protected function getViewData()
    {
        return array();
    }
    
    protected function getEditFormRules($id)
    {
        return array();
    }
    
    protected function getModuleName()
    {
        $controllerName = (new \ReflectionClass($this))->getShortName();
        
        $baseName = str_replace('Controller', '', $controllerName);
        
        return Utils::camelCaseToDash($baseName);
    }
    
    protected function getViewTemplate($template)
    {
        $view = 'admin.' . $this->moduleName . '.' . $template;
        
        if (!view()->exists($view)) {
            $view = 'admin.crud.' . $template;
        }
        
        return $view;
    }

    
    protected function getFilters()
    {
        $filters = Input::get('filters') ?: array();
        
        $cleanFilters = array_filter($filters);
        
        if (array_key_exists('active', $filters)) {
            if ($filters['active'] == -1) {
                unset($cleanFilters['active']);
            } else {
                $cleanFilters['active'] = $filters['active'];
            }
        }
        
        return $cleanFilters;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $filters = $this->getFilters();
        
        $items = $this->model->fetchForListing(
            $filters, 
            Input::get('order_field'), 
            Input::get('order_dir')
        );
                
        $view = view(
            $this->getViewTemplate('index'), 
            [
                'items'     => $items,
                'data'      => $this->getViewData(),
                'filters'   => $filters 
            ]
        );
        
        return $view;
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $item = $this->model->find($id);

        if (!$item) {
            $item = new $this->model;
            $item->id = 0;
        }
        
        $view = view(
            $this->getViewTemplate('edit'), 
            [
                'item' => $item,
                'data' => $this->getViewData(),
            ]            
        );
        
        return $view;
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        
        $rules = $this->getEditFormRules($id);
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {            
            return Redirect::to(route($this->moduleName . '.edit', array('id' => $id)))
                ->withErrors($validator)
                ->withInput(Input::all());            
        } else {            
            $item = $this->model->find($id);
            
            if (!$item) {
                $item = new $this->model;
            }
            
            $item->setFromArray(Input::all());
            
            $item->save();

            // redirect
            Session::flash(
                'message', 
                $id ? 'Item updated successfully!' : 'Item created successfully!'
            );
            
            return Redirect::to(route($this->moduleName . '.index') . '#grid');
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return $this->edit(0);
    }

  
    public function show($id)
    {
        return Redirect::to(route($this->moduleName . '.edit', array('id' => $id)));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $item = $this->model->find($id);
            
            if ($item) {
                $item->delete();
                Session::flash('message', 'Item deleted successfully!');
            }        
        } catch (\Exception $e) {
            Session::flash('message', $e->getMessage());
        }
        
        return Redirect::to(route($this->moduleName . '.index'));
    }
}
