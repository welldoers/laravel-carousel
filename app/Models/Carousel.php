<?php

namespace App\Models;

use App\Lib\Model\Base as Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Carousel
 * 
 * @property int $id
 * @property string $title
 * @property bool $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $carousel_items
 *
 * @package App\Models
 */
class Carousel extends Model
{
	public function carousel_items()
	{
		return $this->hasMany(\App\Models\CarouselItem::class);
	}
    
    public function delete() 
    {
        try {
            DB::beginTransaction();
            
            foreach ($this->carousel_items as $item) {
                $item->delete();
            }
            
            parent::delete();
                        
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollback();
            throw $e;
        }
    }
}
