<?php

namespace App\Models;

use App\Lib\Model\Base as Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;


/**
 * Class CarouselItem
 * 
 * @property int $id
 * @property int $carousel_id
 * @property string $title
 * @property string $title_position
 * @property string $image_extension
 * @property int $order
 * 
 * @property \App\Models\Carousel $carousel
 *
 * @package App\Models
 */
class CarouselItem extends Model
{
    use \App\Lib\Model\Traits\Image;
    
    public function __construct(array $attributes = array()) 
    {
        parent::__construct($attributes);
        
       /**
        * @todo - put sizes & path in configuration file
        */
        $this->setImageSizes(
            array(
                'thumb' => array(
                    'width'     => 150,
                    'height'    => null
                ),
                'medium' => array(
                    'width'     => 400,
                    'height'    => null
                ),
                'big' => array(
                    'width'     => 1200,
                    'height'    => null
                )
            )
        );
        
        $this->setImagePath('images/carousel');
        
        
        $this->imageExtensionProperty = 'image_extension';
        $this->idProperty = 'id';
    }

    public function carousel()
	{
		return $this->belongsTo(\App\Models\Carousel::class);
	}
    
    public function getPositions()
    {
        return array(
            'left'      => 'Left', 
            'right'     => 'Right',
            'center'    => 'Center'
        );
    }
    
    public function save(array $options = array()) {
        
        $image = null;
        
        if ($this->image_extension instanceof UploadedFile) {
            $image = $this->image_extension;
            
            $this->image_extension = $image->getClientOriginalExtension();
        }
        
        try {
            DB::beginTransaction();
            
            parent::save($options);
            
            if ($image) {
                $this->storeImages($image);
            }
            
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollback();
            throw $e;
        }
    }
    
    public function delete() 
    {
        try {
            parent::delete();
            
            $this->clearImages(true);
            
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollback();
            throw $e;
        }
    }
}
